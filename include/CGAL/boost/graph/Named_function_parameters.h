// Redirect required after PR https://github.com/CGAL/cgal/pull/5639
// See patch: https://gist.github.com/MaelRL/62968fbd789150c018b04b93c9daa55d

#ifndef CGAL_BOOST_GRAPH_NAMED_FUNCTION_PARAMS_H
#define CGAL_BOOST_GRAPH_NAMED_FUNCTION_PARAMS_H

#include <CGAL/Named_function_parameters.h>

#define CGAL_BGL_NP_TEMPLATE_PARAMETERS NP_T=bool, typename NP_Tag=CGAL::internal_np::all_default_t, typename NP_Base=CGAL::internal_np::No_property
#define CGAL_BGL_NP_TEMPLATE_PARAMETERS_NO_DEFAULT NP_T, typename NP_Tag, typename NP_Base
#define CGAL_BGL_NP_CLASS CGAL::Named_function_parameters<NP_T,NP_Tag,NP_Base>

namespace CGAL {
namespace Point_set_processing_3 {

template<typename PointRange, typename NamedParameters>
class GetK
{
  typedef typename GetPointMap<PointRange, NamedParameters>::type Vpm;
  typedef typename Kernel_traits<
    typename boost::property_traits<Vpm>::value_type
  >::Kernel Default_kernel;

public:
  typedef typename internal_np::Lookup_named_param_def <
    internal_np::geom_traits_t,
    NamedParameters,
    Default_kernel
  > ::type  Kernel;
};

} // namespace Point_set_processing_3
} // namespace CGAL

#endif // CGAL_BOOST_GRAPH_NAMED_FUNCTION_PARAMS_H
